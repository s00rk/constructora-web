var Verallo = angular.module('verallo');

Verallo.value("SERVER", 'http://localhost');
Verallo.value("storage", localStorage);

Verallo.config(httpProvider);
httpProvider.$inject = ['$resourceProvider', '$httpProvider'];

function httpProvider($resourceProvider, $httpProvider) {
    $resourceProvider.defaults.stripTrailingSlashes = false;
    $httpProvider.defaults.headers.common.Authorization = "Token " + localStorage.getItem('TOKEN');
    $httpProvider.defaults.transformResponse = function(data, headersGetter, status) {
        var responseHeaders = headersGetter();
        var obtener = function(r) {

            $.each(r, function(i, o) {
                if (typeof(o) == 'string') {
                    var separar = o.split('-');
                    if (separar.length == 3 && o.indexOf("T") == -1) {
                        var n = separar[2] + '/' + separar[1] + '/' + separar[0];

                        r[i] = n;
                    } else if (separar.length == 3 && o.indexOf("T") != -1) {
                        var m = separar[2].substring(0, separar[2].indexOf("T"));
                        var n = m + '/' + separar[1] + '/' + separar[0];

                        responseData[i] = n;
                    }
                } else if (typeof(o) == 'object' && o != null) {
                    o = obtener(o);
                }
            });
            return r;
        };
        if (responseHeaders["content-type"] == "application/json" || responseHeaders["content-type"] == "application/octet-stream") {
            var responseData = angular.fromJson(data);
            if (responseData.hasOwnProperty("objects")) {
                var results = responseData.objects;
                results.meta = responseData.meta;

                $.each(results, function(i, o) {
                    if (typeof(o) == 'string') {
                        var separar = o.split('-');
                        if (separar.length == 3 && o.indexOf("T") == -1) {
                            var n = separar[2] + '/' + separar[1] + '/' + separar[0];

                            results[i] = n;
                        } else if (separar.length == 3 && o.indexOf("T") != -1) {
                            var m = separar[2].substring(0, separar[2].indexOf("T"));
                            var n = m + '/' + separar[1] + '/' + separar[0];

                            results[i] = n;
                        }
                    } else if (typeof(o) == 'object' && o != null) {
                        o = obtener(o);
                    }
                });
                return results;
            }else{

                $.each(responseData, function(i, o) {
                    if (typeof(o) == 'string') {
                        var separar = o.split('-');
                        if (separar.length == 3 && o.indexOf("T") == -1) {
                            var n = separar[2] + '/' + separar[1] + '/' + separar[0];

                            responseData[i] = n;
                        } else if (separar.length == 3 && o.indexOf("T") != -1) {
                            var m = separar[2].substring(0, separar[2].indexOf("T"));
                            var n = m + '/' + separar[1] + '/' + separar[0];

                            responseData[i] = n;
                        }
                    } else if (typeof(o) == 'object' && o != null) {
                        o = obtener(o);
                    }
                });
            }
            return responseData;
        }
        return data;
    };
}

Verallo.config(function(blockUIConfig) {
	blockUIConfig.autoInjectBodyBlock = false;
	blockUIConfig.template = '<div class="loading"><div class="ball-pulse-rise"><div></div><div></div><div></div><div></div><div></div></div></div>';
});

Verallo.controller('UserBlockController', UserBlockController);
UserBlockController.$inject = ['$rootScope', 'SERVER', 'PerfilService'];

function UserBlockController($rootScope, SERVER, PerfilService) {
    var user = PerfilService.query({
        me: 'true'
    }).$promise.then(function(res) {
        if (res.length == 1) {
            $rootScope.user = res[0];
        }
    });
    $rootScope.server = 'http://' + SERVER;
    $rootScope.emails = [];
}

Verallo.config(routesConfig);
routesConfig.$inject = ['$stateProvider', '$locationProvider', '$urlRouterProvider', 'RouteHelpersProvider'];

function routesConfig($stateProvider, $locationProvider, $urlRouterProvider, helper) {

    $locationProvider.html5Mode(false);

    $urlRouterProvider.otherwise('/inicio');

    $stateProvider
    .state('app', {
        abstract: true,
        data: {
            pageTitle: 'Login'
        },
        templateUrl: helper.basepath('app.html'),
        resolve: helper.resolveFor('modernizr', 'icons', 'loaders.css', 'ui.select', 'ngDialog', 'ngTagsInput',
            'catalogos/perfiles/services/PerfilService.js', 'sparklines'
            )
    })

    .state('login', {
        url: '/login',
        title: 'Login',
        templateUrl: helper.basepath('usuarios/views/login.html'),
        controller: 'LoginController',
        resolve: helper.resolveFor(
            'usuarios/services/AuthService.js',
            'usuarios/controllers/LoginController.js'
            )
    })

    .state('logout', {
        url: '/logout',
        title: 'Log Out',
        controller: function($scope, $route) {
            localStorage.clear();
            window.location = '/';
        }
    })

    .state('inicio', {
        url: '/inicio',
        title: 'Inicio',
        parent: 'app',
        templateUrl: helper.basepath('inicio/views/home.html'),
        controller: 'HomeController',
        resolve: helper.resolveFor(
            'flot-chart','flot-chart-plugins', 'vector-map', 'vector-map-maps',
            'constructora/presupuestos/services/PresupuestoService.js',
            'inicio/controllers/HomeController.js'
            )
    })

    .state('estados', {
        url: '/estados',
        title: 'Estados',
        parent: 'app',
        templateUrl: helper.basepath('catalogos/estados/views/estados.html'),
        controller: 'EstadoIndexController',
        resolve: helper.resolveFor(
            'catalogos/estados/services/EstadoService.js',
            'catalogos/estados/controllers/EstadoIndexController.js'
            )
    })

    .state('estado_edit', {
        url: '/estados/editar/:id',
        title: 'Editar Estado',
        parent: 'app',
        templateUrl: helper.basepath('catalogos/estados/views/estado-form.html'),
        controller: 'EstadoController',
        resolve: helper.resolveFor(
            'catalogos/estados/services/EstadoService.js',
            'catalogos/estados/controllers/EstadoController.js'
            )
    })

    .state('estado_new', {
        url: '/estados/nuevo',
        title: 'Nuevo Estado',
        parent: 'app',
        templateUrl: helper.basepath('catalogos/estados/views/estado-form.html'),
        controller: 'EstadoController',
        resolve: helper.resolveFor(
            'catalogos/estados/services/EstadoService.js',
            'catalogos/estados/controllers/EstadoController.js'
            )
    })

    .state('municipios', {
        url: '/municipios',
        title: 'Municipios',
        parent: 'app',
        templateUrl: helper.basepath('catalogos/municipios/views/municipios.html'),
        controller: 'MunicipioIndexController',
        resolve: helper.resolveFor(
            'catalogos/municipios/services/MunicipioService.js',
            'catalogos/municipios/controllers/MunicipioIndexController.js'
            )
    })

    .state('municipio_edit', {
        url: '/municipios/editar/:id',
        title: 'Editar Municipio',
        parent: 'app',
        templateUrl: helper.basepath('catalogos/municipios/views/municipio-form.html'),
        controller: 'MunicipioController',
        resolve: helper.resolveFor(
            'catalogos/estados/services/EstadoService.js',
            'catalogos/municipios/services/MunicipioService.js',
            'catalogos/municipios/controllers/MunicipioController.js'
            )
    })

    .state('municipio_new', {
        url: '/municipios/nuevo',
        title: 'Nuevo Municipio',
        parent: 'app',
        templateUrl: helper.basepath('catalogos/municipios/views/municipio-form.html'),
        controller: 'MunicipioController',
        resolve: helper.resolveFor(
            'catalogos/estados/services/EstadoService.js',
            'catalogos/municipios/services/MunicipioService.js',
            'catalogos/municipios/controllers/MunicipioController.js'
            )
    })

    .state('unidad_medidas', {
        url: '/unidad_medidas',
        title: 'Unidades de Medida',
        parent: 'app',
        templateUrl: helper.basepath('catalogos/unidad_medidas/views/unidad_medidas.html'),
        controller: 'UnidadMedidaIndexController',
        resolve: helper.resolveFor(
            'catalogos/unidad_medidas/services/UnidadMedidaService.js',
            'catalogos/unidad_medidas/controllers/UnidadMedidaIndexController.js'
            )
    })

    .state('unidad_medida_edit', {
        url: '/unidad_medidas/editar/:id',
        title: 'Editar Unidad de Medida',
        parent: 'app',
        templateUrl: helper.basepath('catalogos/unidad_medidas/views/unidad_medida-form.html'),
        controller: 'UnidadMedidaController',
        resolve: helper.resolveFor(
            'catalogos/unidad_medidas/services/UnidadMedidaService.js',
            'catalogos/unidad_medidas/controllers/UnidadMedidaController.js'
            )
    })

    .state('unidad_medida_new', {
        url: '/unidad_medidas/nuevo',
        title: 'Nueva Unidad de Medida',
        parent: 'app',
        templateUrl: helper.basepath('catalogos/unidad_medidas/views/unidad_medida-form.html'),
        controller: 'UnidadMedidaController',
        resolve: helper.resolveFor(
            'catalogos/unidad_medidas/services/UnidadMedidaService.js',
            'catalogos/unidad_medidas/controllers/UnidadMedidaController.js'
            )
    })

    .state('proveedores', {
        url: '/proveedores',
        title: 'Proveedores',
        parent: 'app',
        templateUrl: helper.basepath('catalogos/proveedores/views/proveedores.html'),
        controller: 'ProveedorIndexController',
        resolve: helper.resolveFor(
            'catalogos/proveedores/services/ProveedorService.js',
            'catalogos/proveedores/controllers/ProveedorIndexController.js'
            )
    })

    .state('proveedor_edit', {
        url: '/proveedores/editar/:id',
        title: 'Editar Proveedor',
        parent: 'app',
        templateUrl: helper.basepath('catalogos/proveedores/views/proveedor-form.html'),
        controller: 'ProveedorController',
        resolve: helper.resolveFor(
            'catalogos/contactos/services/ContactoService.js',
            'catalogos/proveedores/services/ProveedorService.js',
            'catalogos/proveedores/controllers/ProveedorController.js',
            'catalogos/sucursales/services/SucursalService.js'
            )
    })

    .state('proveedor_new', {
        url: '/proveedores/nuevo',
        title: 'Nuevo Proveedor',
        parent: 'app',
        templateUrl: helper.basepath('catalogos/proveedores/views/proveedor-form.html'),
        controller: 'ProveedorController',
        resolve: helper.resolveFor(
            'catalogos/contactos/services/ContactoService.js',
            'catalogos/proveedores/services/ProveedorService.js',
            'catalogos/proveedores/controllers/ProveedorController.js',
            'catalogos/sucursales/services/SucursalService.js'
            )
    })

    .state('clientes', {
        url: '/clientes',
        title: 'Clientes',
        parent: 'app',
        templateUrl: helper.basepath('catalogos/clientes/views/clientes.html'),
        controller: 'ClienteIndexController',
        resolve: helper.resolveFor(
            'catalogos/clientes/services/ClienteService.js',
            'catalogos/clientes/controllers/ClienteIndexController.js'
            )
    })

    .state('cliente_edit', {
        url: '/clientes/editar/:id',
        title: 'Editar Cliente',
        parent: 'app',
        templateUrl: helper.basepath('catalogos/clientes/views/cliente-form.html'),
        controller: 'ClienteController',
        resolve: helper.resolveFor(
            'catalogos/contactos/services/ContactoService.js',
            'catalogos/clientes/services/ClienteService.js',
            'catalogos/clientes/controllers/ClienteController.js',
            'catalogos/sucursales/services/SucursalService.js'
            )
    })

    .state('cliente_new', {
        url: '/clientes/nuevo',
        title: 'Nuevo Cliente',
        parent: 'app',
        templateUrl: helper.basepath('catalogos/clientes/views/cliente-form.html'),
        controller: 'ClienteController',
        resolve: helper.resolveFor(
            'catalogos/contactos/services/ContactoService.js',
            'catalogos/clientes/services/ClienteService.js',
            'catalogos/clientes/controllers/ClienteController.js',
            'catalogos/sucursales/services/SucursalService.js'
            )
    })

    .state('insumos', {
        url: '/insumos',
        title: 'Insumos',
        parent: 'app',
        templateUrl: helper.basepath('catalogos/insumos/views/insumos.html'),
        controller: 'InsumoIndexController',
        resolve: helper.resolveFor(
            'catalogos/insumos/services/InsumoService.js',
            'catalogos/insumos/controllers/InsumoIndexController.js'
            )
    })

    .state('insumo_edit', {
        url: '/insumos/editar/:id',
        title: 'Editar Insumo',
        parent: 'app',
        templateUrl: helper.basepath('catalogos/insumos/views/insumo-form.html'),
        controller: 'InsumoController',
        resolve: helper.resolveFor(
            'catalogos/proveedores/services/ProveedorService.js',
            'catalogos/insumos/services/InsumoService.js',
            'catalogos/unidad_medidas/services/UnidadMedidaService.js',
            'catalogos/insumos/controllers/InsumoController.js'
            )
    })

    .state('insumo_new', {
        url: '/insumos/nuevo',
        title: 'Nuevo Insumo',
        parent: 'app',
        templateUrl: helper.basepath('catalogos/insumos/views/insumo-form.html'),
        controller: 'InsumoController',
        resolve: helper.resolveFor(
            'catalogos/proveedores/services/ProveedorService.js',
            'catalogos/insumos/services/InsumoService.js',
            'catalogos/unidad_medidas/services/UnidadMedidaService.js',
            'catalogos/insumos/controllers/InsumoController.js'
            )
    })

    .state('manos_obras', {
        url: '/manos_obras',
        title: 'Mano de Obra',
        parent: 'app',
        templateUrl: helper.basepath('catalogos/mano_obra/views/manos_obra.html'),
        controller: 'ManoObraIndexController',
        resolve: helper.resolveFor(
            'catalogos/insumos/services/InsumoService.js',
            'catalogos/mano_obra/controllers/ManoObraIndexController.js'
            )
    })

    .state('mano_obra_edit', {
        url: '/manos_obras/editar/:id',
        title: 'Editar Mano de Obra',
        parent: 'app',
        templateUrl: helper.basepath('catalogos/mano_obra/views/mano_obra-form.html'),
        controller: 'ManoObraController',
        resolve: helper.resolveFor(
            'catalogos/insumos/services/InsumoService.js',
            'catalogos/unidad_medidas/services/UnidadMedidaService.js',
            'catalogos/mano_obra/controllers/ManoObraController.js'
            )
    })

    .state('mano_obra_new', {
        url: '/manos_obras/nuevo',
        title: 'Nueva Mano de Obra',
        parent: 'app',
        templateUrl: helper.basepath('catalogos/mano_obra/views/mano_obra-form.html'),
        controller: 'ManoObraController',
        resolve: helper.resolveFor(
            'catalogos/insumos/services/InsumoService.js',
            'catalogos/unidad_medidas/services/UnidadMedidaService.js',
            'catalogos/mano_obra/controllers/ManoObraController.js'
            )
    })

    .state('correo', {
        url: '/correo',
        title: 'Correo',
        templateUrl: helper.basepath('mailbox/views/index.html'),
        parent: 'app',
        abstract: true,
        controller: 'CorreoIndexController',
        resolve: helper.resolveFor(
            'mailbox/services/CorreoService.js',
            'mailbox/controllers/CorreoIndexController.js'
            )
    })

    .state('correo.folder', {
        url: '/folder/:folder',
        title: 'Correo',
        templateUrl: helper.basepath('mailbox/views/inbox.html'),
        controller: 'CorreoFolderController',
        resolve: helper.resolveFor(
            'mailbox/controllers/CorreoFolderController.js'
            )
    })

    .state('correo.view', {
        url: '/folder/:folder/{mid:[0-9]{1,4}}',
        title: 'Ver correo',
        templateUrl: helper.basepath('mailbox/views/view.html'),
        controller: 'CorreoViewController',
        resolve: helper.resolveFor(
        	'ngWig',
        	'mailbox/controllers/CorreoViewController.js'
            )
    })

    .state('correo.nuevo', {
        url: '/nuevo',
        title: 'Correo Nuevo',
        templateUrl: helper.basepath('mailbox/views/nuevo.html'),
        controller: 'CorreoNuevoController',
        resolve: helper.resolveFor(
        	'ngWig',
        	'mailbox/controllers/CorreoNuevoController.js'
            )
    })

    .state('presupuestos', {
        url: '/presupuestos',
        title: 'Presupuestos',
        parent: 'app',
        templateUrl: helper.basepath('constructora/presupuestos/views/presupuestos.html'),
        controller: 'PresupuestoIndexController',
        resolve: helper.resolveFor(
            'mailbox/services/CorreoService.js',
            'constructora/presupuestos/services/PresupuestoService.js',
            'constructora/presupuestos/controllers/PresupuestoIndexController.js'
            )
    })

    .state('presupuesto_edit', {
        url: '/presupuestos/editar/:id',
        title: 'Editar Presupuesto',
        parent: 'app',
        templateUrl: helper.basepath('constructora/presupuestos/views/presupuesto-form.html'),
        controller: 'PresupuestoController',
        resolve: helper.resolveFor(
            'catalogos/clientes/services/ClienteService.js',
            'constructora/presupuestos/services/PresupuestoService.js',
            'constructora/presupuestos/services/ObraService.js',
            'constructora/presupuestos/services/ObraItemService.js',
            'catalogos/insumos/services/InsumoService.js',
            'catalogos/unidad_medidas/services/UnidadMedidaService.js',
            'constructora/presupuestos/controllers/PresupuestoController.js',
            'catalogos/sucursales/services/SucursalService.js'
            )
    })

    .state('presupuesto_new', {
        url: '/presupuestos/nuevo',
        title: 'Nuevo Presupuesto',
        parent: 'app',
        templateUrl: helper.basepath('constructora/presupuestos/views/presupuesto-form.html'),
        controller: 'PresupuestoController',
        resolve: helper.resolveFor(
            'catalogos/clientes/services/ClienteService.js',
            'constructora/presupuestos/services/PresupuestoService.js',
            'constructora/presupuestos/services/ObraService.js',
            'constructora/presupuestos/services/ObraItemService.js',
            'catalogos/insumos/services/InsumoService.js',
            'catalogos/unidad_medidas/services/UnidadMedidaService.js',
            'constructora/presupuestos/controllers/PresupuestoController.js',
            'catalogos/sucursales/services/SucursalService.js'
            )
    });

};