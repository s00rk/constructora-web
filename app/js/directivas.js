var Verallo = angular.module('verallo');

Verallo.directive('ngCancel', function(){
	return {
		restrict: 'A',

		link: function(scope, element, attrs) {
			element.bind('click', goBack);

			function goBack() {
				history.back();
				scope.$apply();
			}
		}
	}
});

Verallo.directive('stringToNumber', function() {
	return {
		require: 'ngModel',
		link: function(scope, element, attrs, ngModel) {
			ngModel.$parsers.push(function(value) {
				return '' + value;
			});
			ngModel.$formatters.push(function(value) {
				return parseFloat(value, 10);
			});
		}
	};
});

Verallo.directive('ngDate', function() {
    return function(scope, element, attrs) {
        var el = $(element);
        el.datepicker({
            format: 'dd/mm/yyyy',
            language: 'es',
            autoclose: true,
            todayBtn: 'linked'
        });
        el.datepicker('setDate', new Date());
        if(attrs.endDate != undefined)
        {
            end = new Date();
            end.setDate(end.getDate() + parseInt(attrs.endDate));
            el.datepicker('setEndDate', end );
        }
    };
});

Verallo.directive('stGroup', function() {
	return {
		restrict: 'A',
		require: '^stTable',
		link: function(scope, element, attrs, ctrl) {
			var groupKey = attrs['stGroup'];
			var groupKey2 = attrs['stGroupKey'];

			var buildGroupedData = function(data) {
				var result = [];

				for (var i = 0; i < data.length; i++) {
					var item = data[i];
					var group = undefined;

					var key = item[groupKey][groupKey2];
					for (var j = 0; j < result.length; j++) {
						if (group === undefined && result[j].key === key) {
							group = result[j];
						}
					}
					if (!group) {
						group = {
							key: key,
							items: []
						};

						result.push(group);
					}

					group.items.push(item);
				}
				return result;
			};

			scope.$watch(function() {
				return scope[attrs['stTable']];
			}, function(newValue, oldValue) {
				if (newValue) {
					scope.groupedData = buildGroupedData(newValue);
				}
			});
		}
	};
});