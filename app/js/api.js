angular.module('verallo').factory('ApiServices', ['SERVER', function(SERVER) {
	var urlBase = SERVER + '/';
	return {
		get: function(nombreRecurso){          
			return urlBase + "api/v1/" + nombreRecurso + "/:id/";			
		},
		auth: urlBase + "auth/"
	}
}]);
