var app = angular.module("verallo");
app.factory("MunicipioService", MunicipioService);
MunicipioService.$injects = ['$resource', 'ApiServices'];
function MunicipioService($resource, ApiServices){
	return $resource(ApiServices.get('municipios'),
		{
			id: "@id"
		},
		{
			'update': { method:'PUT' }
		}
	);
};