angular.module('verallo').controller('MunicipioIndexController', MunicipioIndexController);

MunicipioIndexController.$inject = ['toaster', '$rootScope', '$state', '$scope', 'storage', 'MunicipioService'];
function MunicipioIndexController(toaster, $rootScope, $state, $scope, storage, MunicipioService) {
	$scope.municipios = [];
	
	$scope.callServer = function(tableState) {
		var pagination = tableState.pagination;

		var start = pagination.start || 0;
		var number = pagination.number || 10;
		var data = {offset: start, limit: number};
		if("predicateObject" in tableState.search)
		{
			data['query'] = tableState.search.predicateObject.$;
		}
		MunicipioService.query(data).$promise.then(function (result) {
			$scope.municipios = result;			
			tableState.pagination.numberOfPages = Math.ceil(result.meta.total_count / number);

		});
	};

	$scope.edit = function(municipio)
	{
		$state.go('municipio_edit', {id: municipio.id});
	};

	$scope.new = function()
	{
		$state.go('municipio_new');
	};
	
	$scope.remove = function(index, municipio)
	{
		MunicipioService.delete({ id: municipio.id });
		$scope.municipios.splice(index, 1);
		toaster.pop('success', 'Eliminar', 'Se ha eliminado el Municipio');
	};
}