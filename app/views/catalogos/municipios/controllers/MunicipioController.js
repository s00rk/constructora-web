angular.module('verallo').controller('MunicipioController', MunicipioController);

MunicipioController.$inject = ['toaster', '$stateParams', '$rootScope', '$state', '$scope', 'storage', 'EstadoService', 'MunicipioService'];
function MunicipioController(toaster, $stateParams, $rootScope, $state, $scope, storage, EstadoService, MunicipioService) {
	$scope.titulo = "Nuevo Municipio";

	$scope.estados = EstadoService.query();

	if($stateParams.id != undefined)
	{
		MunicipioService.get({ id: $stateParams.id }).$promise.then(function(res){
			$scope.municipio = res;
			$scope.titulo = "Editar Municipio: " + res.nombre;
		});
	}else{
		$scope.municipio = new MunicipioService();
	}

	var exito = function(data)
	{
		toaster.pop('success', 'Guardar', 'Se ha guardado el Municipio');
		$state.go('municipios');
	};

	var error = function(data)
	{
		$.each(data.data, function(i, obj){
			$.each(obj, function(ii, objj){
				$.each(objj, function(j, k){
					toaster.pop('error', (ii.substr(0,1).toUpperCase()+ii.substr(1)), k);
				});
			});
		});
	};

	$scope.guardar = function()
	{
		if($scope.frmMunicipio.$valid)
		{
			if($scope.municipio.id == undefined)
			{
				$scope.municipio.$save({}, exito, error);
			}else{
				$scope.municipio.$update({}, exito, error);
			}
		}
	};
}