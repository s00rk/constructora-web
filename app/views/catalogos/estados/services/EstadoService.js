var app = angular.module("verallo");
app.factory("EstadoService", EstadoService);
EstadoService.$injects = ['$resource', 'ApiServices'];
function EstadoService($resource, ApiServices){
	return $resource(ApiServices.get('estados'),
		{
			id: "@id"
		},
		{
			'update': { method:'PUT' }
		}
	);
};