angular.module('verallo').controller('EstadoIndexController', EstadoIndexController);

EstadoIndexController.$inject = ['toaster', '$rootScope', '$state', '$scope', 'storage', 'EstadoService'];
function EstadoIndexController(toaster, $rootScope, $state, $scope, storage, EstadoService) {
	$scope.estados = [];
	
	$scope.callServer = function(tableState) {
		var pagination = tableState.pagination;

		var start = pagination.start || 0;
		var number = pagination.number || 10;
		var data = {offset: start, limit: number};
		if("predicateObject" in tableState.search)
		{
			data['nombre__icontains'] = tableState.search.predicateObject.$;
		}
		EstadoService.query(data).$promise.then(function (result) {
			$scope.estados = result;			
			tableState.pagination.numberOfPages = Math.ceil(result.meta.total_count / number);

		});
	};

	$scope.edit = function(proveedor)
	{
		$state.go('estado_edit', {id: proveedor.id});
	};

	$scope.new = function()
	{
		$state.go('estado_new');
	};
	
	$scope.remove = function(index, proveedor)
	{
		EstadoService.delete({ id: proveedor.id });
		$scope.estados.splice(index, 1);
		toaster.pop('success', 'Eliminar', 'Se ha eliminado el Estado');
	};
}