angular.module('verallo').controller('EstadoController', EstadoController);

EstadoController.$inject = ['toaster', '$stateParams', '$rootScope', '$state', '$scope', 'storage', 'EstadoService'];
function EstadoController(toaster, $stateParams, $rootScope, $state, $scope, storage, EstadoService) {
	$scope.titulo = "Nuevo Estado";

	if($stateParams.id != undefined)
	{
		EstadoService.get({ id: $stateParams.id }).$promise.then(function(res){
			$scope.estado = res;
			$scope.titulo = "Editar Estado: " + res.nombre;
		});
	}else{
		$scope.estado = new EstadoService();
	}

	var exito = function(data)
	{
		toaster.pop('success', 'Guardar', 'Se ha guardado el Estado');
		$state.go('estados');
	};

	var error = function(data)
	{
		$.each(data.data, function(i, obj){
			$.each(obj, function(ii, objj){
				$.each(objj, function(j, k){
					toaster.pop('error', (ii.substr(0,1).toUpperCase()+ii.substr(1)), k);
				});
			});
		});
	};

	$scope.guardar = function()
	{
		if($scope.frmEstado.$valid)
		{
			if($scope.estado.id == undefined)
			{
				$scope.estado.$save({}, exito, error);
			}else{
				$scope.estado.$update({}, exito, error);
			}
		}
	};
}