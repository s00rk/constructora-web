angular.module('verallo').controller('ManoObraController', ManoObraController);

ManoObraController.$inject = ['toaster', '$stateParams', '$state', '$scope', 'InsumoService', 'UnidadMedidaService'];
function ManoObraController(toaster, $stateParams, $state, $scope, InsumoService, UnidadMedidaService) {
	$scope.titulo = "Nueva Mano de Obra";

	$scope.unidad_medidas = UnidadMedidaService.query();

	if($stateParams.id != undefined)
	{
		InsumoService.get({ id: $stateParams.id }).$promise.then(function(res){
			$scope.insumo = res;
			$scope.insumo.precio = parseFloat(res.precio);
			$scope.titulo = "Editar Mano de Obra: " + res.nombre;
		});
	}else{
		$scope.insumo = new InsumoService();
		UnidadMedidaService.query({ codigo: 'JOR' }).$promise.then(function(res){
			$scope.insumo.unidad_medida = res[0];
		});
	}
	
	$scope.$watch('insumo.costo', function(n, o){
		if($scope.insumo.costo == undefined)
			return;
		var precio = $scope.insumo.costo;
		if(precio == undefined || isNaN(precio))
			return;
		var suma = 0;
		var e = 1.2717 * precio;
		suma = Math.round((e*0.01425) * 100) / 100;
		suma += Math.round((e*0.0758875) * 100) / 100;
		suma += Math.round((0.2040*70.1) * 100) / 100;
		suma += Math.round(((e-(3*70.1))*0.0150) * 100) / 100;
		suma += Math.round((e*0.0095) * 100) / 100;
		suma += Math.round((e*0.02375) * 100) / 100;
		suma += Math.round((e*0.02) * 100) / 100;
		suma += Math.round((e*0.04275) * 100) / 100;
		suma += Math.round((e*0.01) * 100) / 100;
		suma += Math.round((e*0.05) * 100) / 100;
		$scope.insumo.precio = Math.round((suma+e) * 100) / 100;
	});

	var exito = function(data)
	{
		toaster.pop('success', 'Guardar', 'Se ha guardado la Mano de Obra');
		$state.go('manos_obras');
	};

	var error = function(data)
	{
		$.each(data.data, function(i, obj){
			$.each(obj, function(ii, objj){
				$.each(objj, function(j, k){
					toaster.pop('error', (ii.substr(0,1).toUpperCase()+ii.substr(1)), k);
				});
			});
		});
	};

	$scope.guardar = function()
	{
		if($scope.frmInsumo.$valid)
		{
			if($scope.insumo.id == undefined)
			{
				$scope.insumo.$save({}, exito, error);
			}else{
				$scope.insumo.$update({}, exito, error);
			}
		}
	};
}