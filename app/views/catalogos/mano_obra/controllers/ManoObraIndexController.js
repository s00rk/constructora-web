angular.module('verallo').controller('ManoObraIndexController', ManoObraIndexController);

ManoObraIndexController.$inject = ['toaster', '$rootScope', '$state', '$scope', 'storage', 'InsumoService'];
function ManoObraIndexController(toaster, $rootScope, $state, $scope, storage, InsumoService) {
	$scope.insumos = [];
	
	$scope.callServer = function(tableState) {
		var pagination = tableState.pagination;

		var start = pagination.start || 0;
		var number = pagination.number || 10;
		var data = {offset: start, limit: number};
		
		if("predicateObject" in tableState.search)
		{
			data['query'] = tableState.search.predicateObject.$;
		}
		data['unidad_medida__codigo'] = 'JOR';
		InsumoService.query(data).$promise.then(function (result) {
			$scope.insumos = result;
			tableState.pagination.numberOfPages = Math.ceil(result.meta.total_count / number);

		});
	};

	$scope.edit = function(insumo)
	{
		$state.go('mano_obra_edit', {id: insumo.id});
	};

	$scope.new = function()
	{
		$state.go('mano_obra_new');
	};
	
	$scope.remove = function(index, insumo)
	{
		InsumoService.delete({ id: insumo.id });
		$scope.insumos.splice(index, 1);
		toaster.pop('success', 'Eliminar', 'Se ha eliminado la Mano de Obra');
		$state.go('manos_obras');
	};
}