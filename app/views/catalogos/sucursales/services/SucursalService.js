var app = angular.module("verallo");
app.factory("SucursalService", SucursalService);
SucursalService.$injects = ['$resource', 'ApiServices'];
function SucursalService($resource, ApiServices){
	return $resource(ApiServices.get('sucursales'),
		{
			id: "@id"
		},
		{
			'update': { method:'PUT' }
		}
	);
};