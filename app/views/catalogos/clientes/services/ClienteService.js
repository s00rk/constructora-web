var app = angular.module("verallo");
app.factory("ClienteService", ClienteService);
ClienteService.$injects = ['$resource', 'ApiServices'];
function ClienteService($resource, ApiServices){
	return $resource(ApiServices.get('clientes'),
		{
			id: "@id"
		},
		{
			'update': { method:'PUT' }
		}
	);
};