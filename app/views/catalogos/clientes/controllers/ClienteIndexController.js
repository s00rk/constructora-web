angular.module('verallo').controller('ClienteIndexController', ClienteIndexController);

ClienteIndexController.$inject = ['toaster', '$rootScope', '$state', '$scope', 'storage', 'ClienteService'];
function ClienteIndexController(toaster, $rootScope, $state, $scope, storage, ClienteService) {
	$scope.clientes = [];
	
	$scope.callServer = function(tableState) {
		var pagination = tableState.pagination;

		var start = pagination.start || 0;
		var number = pagination.number || 10;
		var data = {offset: start, limit: number};
		if("predicateObject" in tableState.search)
		{
			data['razon_social__icontains'] = tableState.search.predicateObject.$;
		}
		ClienteService.query(data).$promise.then(function (result) {
			$scope.clientes = result;			
			tableState.pagination.numberOfPages = Math.ceil(result.meta.total_count / number);

		});
	};

	$scope.edit = function(cliente)
	{
		$state.go('cliente_edit', {id: cliente.id});
	};
	
	$scope.new = function()
	{
		$state.go('cliente_new');
	};

	$scope.remove = function(index, cliente)
	{
		ClienteService.delete({ id: cliente.id });
		$scope.clientes.splice(index, 1);
		toaster.pop('success', 'Eliminar', 'Se ha eliminado el Cliente');
	};
}