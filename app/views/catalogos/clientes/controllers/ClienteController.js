angular.module('verallo').controller('ClienteController', ClienteController);

ClienteController.$inject = ['toaster', 'ngDialog', '$rootScope', '$state', '$scope', '$stateParams', 'storage', 
'ClienteService', 'ContactoService', 'SucursalService'];
function ClienteController(toaster, ngDialog, $rootScope, $state, $scope, $stateParams, storage, ClienteService, 
	ContactoService, SucursalService) {
	$scope.titulo = 'Nuevo Cliente';
	
	if($stateParams.id)
	{
		ClienteService.get({ id: $stateParams.id }).$promise.then(function(res){
			$scope.cliente = res;
			$scope.titulo = 'Editar Cliente: ' + res.razon_social;	
		});
	}else{
		$scope.cliente = new ClienteService();
		$scope.cliente.contactos = [];
		$scope.cliente.sucursales = [];
	}

	$scope.addS = function()
	{
		$scope.cliente.sucursales.push({});
	}
	$scope.removeS = function(index, suc)
	{
		if(suc.id != undefined)
			SucursalService.delete({ id: suc.id });
		$scope.cliente.sucursales.splice(index, 1);
	}

	var exito = function(data)
	{
		toaster.pop('success', 'Guardar', 'Se ha guardado el Cliente');
		$state.go('clientes');
	};
	var exito_c = function(data)
	{
		toaster.pop('success', 'Guardar', 'Se ha guardado el Contacto');
	};

	var error = function(data)
	{
		$.each(data.data, function(i, obj){
			$.each(obj, function(ii, objj){
				$.each(objj, function(j, k){
					toaster.pop('error', (ii.substr(0,1).toUpperCase()+ii.substr(1)), k);
				});
			});
		});
	};

	var abrirModal = function(index, contacto)
	{
		ngDialog.open({
			template: 'app/views/catalogos/clientes/views/cliente-contacto-form.html',
			scope: $scope,
			controller: function($scope)
			{
				$scope.titulo_c = 'Nuevo Contacto';
				$scope.nuevo_contacto = {};
				if(contacto != null)
				{
					var c = angular.copy(contacto);
					$scope.nuevo_contacto = contacto;
					$scope.titulo_c = 'Editar Contacto: ' + c.nombre;
				}

				$scope.guardarContacto = function()
				{
					if($scope.frmContacto.$invalid)
						return;
					if(index != null)
					{
						$scope.cliente.contactos.splice(index, 1)
					}
					$scope.cliente.contactos.push( $scope.nuevo_contacto );
					ngDialog.close();
				}

				$scope.cerrar = function()
				{
					ngDialog.close();
				}
			}
		});
	}

	$scope.new = function()
	{
		abrirModal(null, null);
	}

	$scope.remove = function(index, contacto)
	{
		if(contacto.id)
		{
			ContactoService.delete({ id: contacto.id });
		}
		$scope.cliente.contactos.splice(index, 1);
		toaster.pop('success', 'Eliminar', 'Se ha eliminado el Contacto');
	}

	$scope.edit = function(index, contacto)
	{
		abrirModal(index, contacto);
	}

	$scope.guardar = function()
	{	
		if($scope.frmCliente.$valid)
		{
			if($scope.cliente.id == undefined)
			{
				$scope.cliente.$save({}, exito, error);
			}else{
				$scope.cliente.$update({}, exito, error);
			}
		}
	}
}