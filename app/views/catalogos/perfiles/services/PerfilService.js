var app = angular.module("verallo");
app.factory("PerfilService", PerfilService);
PerfilService.$injects = ['$resource', 'ApiServices'];
function PerfilService($resource, ApiServices){
	return $resource(ApiServices.get('perfiles'),
		{
			id: "@id"
		},
		{
			'update': { method:'PUT' }
		}
	);
};