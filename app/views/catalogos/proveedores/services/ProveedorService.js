var app = angular.module("verallo");
app.factory("ProveedorService", ProveedorService);
ProveedorService.$injects = ['$resource', 'ApiServices'];
function ProveedorService($resource, ApiServices){
	return $resource(ApiServices.get('proveedores'),
		{
			id: "@id"
		},
		{
			'update': { method:'PUT' }
		}
	);
};