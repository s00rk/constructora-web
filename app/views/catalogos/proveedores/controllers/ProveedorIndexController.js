angular.module('verallo').controller('ProveedorIndexController', ProveedorIndexController);

ProveedorIndexController.$inject = ['toaster', '$rootScope', '$state', '$scope', 'storage', 'ProveedorService'];
function ProveedorIndexController(toaster, $rootScope, $state, $scope, storage, ProveedorService) {
	$scope.proveedores = [];
	
	$scope.callServer = function(tableState) {
		var pagination = tableState.pagination;

		var start = pagination.start || 0;
		var number = pagination.number || 10;
		var data = {offset: start, limit: number};
		if("predicateObject" in tableState.search)
		{
			data['razon_social__icontains'] = tableState.search.predicateObject.$;
		}
		ProveedorService.query(data).$promise.then(function (result) {
			$scope.proveedores = result;			
			tableState.pagination.numberOfPages = Math.ceil(result.meta.total_count / number);

		});
	};

	$scope.edit = function(proveedor)
	{
		$state.go('proveedor_edit', {id: proveedor.id});
	};
	
	$scope.new = function()
	{
		$state.go('proveedor_new');
	};

	$scope.remove = function(index, proveedor)
	{
		ProveedorService.delete({ id: proveedor.id });
		$scope.proveedores.splice(index, 1);
		toaster.pop('success', 'Eliminar', 'Se ha eliminado el Proveedor');
	};
}