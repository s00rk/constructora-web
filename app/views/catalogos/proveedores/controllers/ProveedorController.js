angular.module('verallo').controller('ProveedorController', ProveedorController);

ProveedorController.$inject = ['toaster', 'ngDialog', '$rootScope', '$state', '$scope', '$stateParams', 'storage', 'ProveedorService', 'ContactoService'];
function ProveedorController(toaster, ngDialog, $rootScope, $state, $scope, $stateParams, storage, ProveedorService, ContactoService) {
	$scope.titulo = 'Nuevo Proveedor';
	
	if($stateParams.id)
	{
		ProveedorService.get({ id: $stateParams.id }).$promise.then(function(res){
			$scope.proveedor = res;
			$scope.titulo = 'Editar Proveedor: ' + res.razon_social;	
		});
	}else{
		$scope.proveedor = new ProveedorService();
		$scope.proveedor.contactos = [];
		$scope.proveedor.sucursales = [];
	}

	$scope.addS = function()
	{
		$scope.proveedor.sucursales.push({});
	}
	$scope.removeS = function(index, suc)
	{
		if(suc.id != undefined)
			SucursalService.delete({ id: suc.id });
		$scope.proveedor.sucursales.splice(index, 1);
	}
	
	var exito = function(data)
	{
		toaster.pop('success', 'Guardar', 'Se ha guardado el Proveedor');
		$state.go('proveedores');
	};
	var exito_c = function(data)
	{
		toaster.pop('success', 'Guardar', 'Se ha guardado el Contacto');
	};

	var error = function(data)
	{
		$.each(data.data, function(i, obj){
			$.each(obj, function(ii, objj){
				$.each(objj, function(j, k){
					toaster.pop('error', (ii.substr(0,1).toUpperCase()+ii.substr(1)), k);
				});
			});
		});
	};

	var abrirModal = function(index, contacto)
	{
		ngDialog.open({
			template: 'app/views/catalogos/proveedores/views/proveedor-contacto-form.html',
			scope: $scope,
			controller: function($scope)
			{
				$scope.titulo_c = 'Nuevo Contacto';
				$scope.nuevo_contacto = {};
				if(contacto != null)
				{
					var c = angular.copy(contacto);
					$scope.nuevo_contacto = contacto;
					$scope.titulo_c = 'Editar Contacto: ' + c.nombre;
				}

				$scope.guardarContacto = function()
				{
					if($scope.frmContacto.$invalid)
						return;
					if(index != null)
					{
						$scope.proveedor.contactos.splice(index, 1)
					}
					$scope.proveedor.contactos.push( $scope.nuevo_contacto );
					ngDialog.close();
				}

				$scope.cerrar = function()
				{
					ngDialog.close();
				}
			}
		});
	}

	$scope.new = function()
	{
		abrirModal(null, null);
	}

	$scope.remove = function(index, contacto)
	{
		if(contacto.id)
		{
			ContactoService.delete({ id: contacto.id });
		}
		$scope.proveedor.contactos.splice(index, 1);
		toaster.pop('success', 'Eliminar', 'Se ha eliminado el Contacto');
	}

	$scope.edit = function(index, contacto)
	{
		abrirModal(index, contacto);
	}

	$scope.guardar = function()
	{	
		if($scope.frmProveedor.$valid)
		{
			if($scope.proveedor.id == undefined)
			{
				$scope.proveedor.$save({}, exito, error);
			}else{
				$scope.proveedor.$update({}, exito, error);
			}
		}
	}
}