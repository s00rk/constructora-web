angular.module('verallo').controller('InsumoIndexController', InsumoIndexController);

InsumoIndexController.$inject = ['toaster', '$rootScope', '$state', '$scope', 'storage', 'InsumoService'];
function InsumoIndexController(toaster, $rootScope, $state, $scope, storage, InsumoService) {
	$scope.insumos = [];
	
	$scope.callServer = function(tableState) {
		var pagination = tableState.pagination;

		var start = pagination.start || 0;
		var number = pagination.number || 10;
		var data = {offset: start, limit: number};
		
		if("predicateObject" in tableState.search)
		{
			data['query'] = tableState.search.predicateObject.$;
		}
		data['!unidad_medida__codigo'] = 'JOR';
		InsumoService.query(data).$promise.then(function (result) {
			$scope.insumos = result;
			tableState.pagination.numberOfPages = Math.ceil(result.meta.total_count / number);

		});
	};

	$scope.edit = function(insumo)
	{
		$state.go('insumo_edit', {id: insumo.id});
	};

	$scope.new = function()
	{
		$state.go('insumo_new');
	};
	
	$scope.remove = function(index, insumo)
	{
		InsumoService.delete({ id: insumo.id });
		$scope.insumos.splice(index, 1);
		toaster.pop('success', 'Eliminar', 'Se ha eliminado el Insumo');
		$state.go('insumos');
	};
}