angular.module('verallo').controller('InsumoController', InsumoController);

InsumoController.$inject = ['toaster', '$stateParams', '$state', '$scope', 'InsumoService', 'ProveedorService', 'UnidadMedidaService'];
function InsumoController(toaster, $stateParams, $state, $scope, InsumoService, ProveedorService, UnidadMedidaService) {
	$scope.titulo = "Nuevo Insumo";

	$scope.proveedores = ProveedorService.query();
	$scope.unidad_medidas = UnidadMedidaService.query();

	if($stateParams.id != undefined)
	{
		InsumoService.get({ id: $stateParams.id }).$promise.then(function(res){
			$scope.insumo = res;
			$scope.insumo.precio = parseFloat(res.precio);
			$scope.titulo = "Editar Insumo: " + res.nombre;
		});
	}else{
		$scope.insumo = new InsumoService();
	}

	var exito = function(data)
	{
		toaster.pop('success', 'Guardar', 'Se ha guardado el Insumo');
		$state.go('insumos');
	};

	var error = function(data)
	{
		$.each(data.data, function(i, obj){
			$.each(obj, function(ii, objj){
				$.each(objj, function(j, k){
					toaster.pop('error', (ii.substr(0,1).toUpperCase()+ii.substr(1)), k);
				});
			});
		});
	};

	$scope.guardar = function()
	{
		if($scope.frmInsumo.$valid)
		{
			if($scope.insumo.id == undefined)
			{
				$scope.insumo.$save({}, exito, error);
			}else{
				$scope.insumo.$update({}, exito, error);
			}
		}
	};
}