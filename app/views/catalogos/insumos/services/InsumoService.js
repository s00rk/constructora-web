var app = angular.module("verallo");
app.factory("InsumoService", InsumoService);
InsumoService.$injects = ['$resource', 'ApiServices'];
function InsumoService($resource, ApiServices){
	return $resource(ApiServices.get('insumos'),
		{
			id: "@id"
		},
		{
			'update': { method:'PUT' }
		}
	);
};