var app = angular.module("verallo");
app.factory("ContactoService", ContactoService);
ContactoService.$injects = ['$resource', 'ApiServices'];
function ContactoService($resource, ApiServices){
	return $resource(ApiServices.get('contactos'),
		{
			id: "@id"
		},
		{
			'update': { method:'PUT' }
		}
	);
};