var app = angular.module("verallo");
app.factory("UnidadMedidaService", UnidadMedidaService);
UnidadMedidaService.$injects = ['$resource', 'ApiServices'];
function UnidadMedidaService($resource, ApiServices){
	return $resource(ApiServices.get('unidad_medidas'),
		{
			id: "@id"
		},
		{
			'update': { method:'PUT' }
		}
	);
};