angular.module('verallo').controller('UnidadMedidaIndexController', UnidadMedidaIndexController);

UnidadMedidaIndexController.$inject = ['toaster', '$rootScope', '$state', '$scope', 'storage', 'UnidadMedidaService'];
function UnidadMedidaIndexController(toaster, $rootScope, $state, $scope, storage, UnidadMedidaService) {
	$scope.unidad_medidas = [];
	
	$scope.callServer = function(tableState) {
		var pagination = tableState.pagination;

		var start = pagination.start || 0;
		var number = pagination.number || 10;
		var data = {offset: start, limit: number};
		if("predicateObject" in tableState.search)
		{
			data['query'] = tableState.search.predicateObject.$;
		}
		UnidadMedidaService.query(data).$promise.then(function (result) {
			$scope.unidad_medidas = result;			
			tableState.pagination.numberOfPages = Math.ceil(result.meta.total_count / number);

		});
	};

	$scope.edit = function(unidad_medida)
	{
		$state.go('unidad_medida_edit', {id: unidad_medida.id});
	};

	$scope.new = function()
	{
		$state.go('unidad_medida_new');
	};
	
	$scope.remove = function(index, unidad_medida)
	{
		UnidadMedidaService.delete({ id: unidad_medida.id });
		$scope.unidad_medidas.splice(index, 1);
		toaster.pop('success', 'Eliminar', 'Se ha eliminado la Unidad de Medida');
	};
}