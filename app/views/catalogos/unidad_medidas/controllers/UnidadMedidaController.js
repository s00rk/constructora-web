angular.module('verallo').controller('UnidadMedidaController', UnidadMedidaController);

UnidadMedidaController.$inject = ['toaster', '$stateParams', '$rootScope', '$state', '$scope', 'storage', 'UnidadMedidaService'];
function UnidadMedidaController(toaster, $stateParams, $rootScope, $state, $scope, storage, UnidadMedidaService) {
	$scope.titulo = "Nueva Unidad de Medida";

	if($stateParams.id != undefined)
	{
		UnidadMedidaService.get({ id: $stateParams.id }).$promise.then(function(res){
			$scope.unidad_medida = res;
			$scope.titulo = "Editar Unidad de Medida: " + res.nombre;
		});
	}else{
		$scope.unidad_medida = new UnidadMedidaService();
	}

	var exito = function(data)
	{
		toaster.pop('success', 'Guardar', 'Se ha guardado la Unidad de Medida');
		$state.go('unidad_medidas');
	};

	var error = function(data)
	{
		$.each(data.data, function(i, obj){
			$.each(obj, function(ii, objj){
				$.each(objj, function(j, k){
					toaster.pop('error', (ii.substr(0,1).toUpperCase()+ii.substr(1)), k);
				});
			});
		});
	};

	$scope.guardar = function()
	{
		if($scope.frmUnidadMedida.$valid)
		{
			if($scope.unidad_medida.id == undefined)
			{
				$scope.unidad_medida.$save({}, exito, error);
			}else{
				$scope.unidad_medida.$update({}, exito, error);
			}
		}
	};
}