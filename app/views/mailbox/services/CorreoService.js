var app = angular.module("verallo");
app.factory("CorreoService", CorreoService);
CorreoService.$injects = ['$resource', 'ApiServices'];
function CorreoService($resource, ApiServices){
	return $resource(ApiServices.get('emails'),
		{
			id: "@id"
		},
		{
			'update': { method:'PUT' }
		}
	);
};