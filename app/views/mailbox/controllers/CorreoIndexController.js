angular.module('verallo').controller('CorreoIndexController', CorreoIndexController);
CorreoIndexController.$inject = ['toaster', '$stateParams', '$state', '$scope'];

function CorreoIndexController(toaster, $stateParams, $state, $scope) {

    $scope.folders = [{
        name: 'Buzón',
        folder: 'inbox',
        icon: 'fa-inbox'
    },  {
        name: 'Enviados',
        folder: 'sent',
        icon: 'fa-paper-plane-o'
    },  {
        name: 'Eliminados',
        folder: 'trash',
        icon: 'fa-trash'
    }];
}