angular.module('verallo').controller('CorreoFolderController', CorreoFolderController);
CorreoFolderController.$inject = ['toaster', '$rootScope', '$stateParams', '$state', '$scope', 'CorreoService'];

function CorreoFolderController(toaster, $rootScope, $stateParams, $state, $scope, CorreoService) {
    $scope.folder = {};
    $scope.folder.folder = $stateParams.folder === 'inbox' ? '' : $stateParams.folder;
    $scope.page = {};

    $scope.page.current_page = 1;
    $scope.page.total_count = 0;
    $scope.page.maxSize = 10;
    $scope.page.numPages = 0;

    CorreoService.query({
        'mailbox': $scope.folder.folder,
        'page': $scope.page.current_page
    }, function(res) {
        $scope.correos = res;

        $.each(res, function(i, obj) {
            var desde = obj.desde.split(',');
            for (var i = 0; i < desde.length; i++) {
                var e = desde[i].split('<');
                if (e.length > 1) {
                    e = e[1].replace('>', '');
                    if ($rootScope.emails.indexOf(e) == -1 && e != $rootScope.user.outlook_user)
                        $rootScope.emails.push(e)
                }else{
                    e = e[0];
                    if ($rootScope.emails.indexOf(e) == -1 && e != $rootScope.user.outlook_user)
                        $rootScope.emails.push(e)
                }
            }

            var para = obj.para.split(',');
            for (var i = 0; i < para.length; i++) {
                var e = para[i].split('<');
                if (e.length > 1) {
                    e = e[1].replace('>', '');
                    if ($rootScope.emails.indexOf(e) == -1 && e != $rootScope.user.outlook_user)
                        $rootScope.emails.push(e)
                } else {
                    e = e[0];
                    if ($rootScope.emails.indexOf(e) == -1 && e != $rootScope.user.outlook_user)
                        $rootScope.emails.push(e)
                }
            }
        });


        $scope.page.total_count = res.meta.total_count;
        $scope.page.numPages = ($scope.page.total_count + $scope.page.maxSize - 1) / $scope.page.maxSize;;
    }, function(res) {
        if (res.data.exito == false) {
            toaster.pop('error', '', 'Usuario y/o Contraseña de Correo Incorrecta');
            return;
        }
    });

    $scope.pageChanged = function() {
        CorreoService.query({
            'mailbox': $scope.folder.folder,
            'page': $scope.page.current_page
        }).$promise.then(function(res) {
            $scope.correos = res;

            $.each(res, function(i, obj) {
                var desde = obj.desde.split(',');
            for (var i = 0; i < desde.length; i++) {
                var e = desde[i].split('<');
                if (e.length > 1) {
                    e = e[1].replace('>', '');
                    if ($rootScope.emails.indexOf(e) == -1)
                        $rootScope.emails.push(e)
                }else{
                    e = e[0];
                    if ($rootScope.emails.indexOf(e) == -1)
                        $rootScope.emails.push(e)
                }
            }

            var para = obj.para.split(',');
            for (var i = 0; i < para.length; i++) {
                var e = para[i].split('<');
                if (e.length > 1) {
                    e = e[1].replace('>', '');
                    if ($rootScope.emails.indexOf(e) == -1)
                        $rootScope.emails.push(e)
                } else {
                    e = e[0];
                    if ($rootScope.emails.indexOf(e) == -1)
                        $rootScope.emails.push(e)
                }
            }

            });
            $scope.page.total_count = res.meta.total_count;
            $scope.page.numPages = ($scope.page.total_count + $scope.page.maxSize - 1) / $scope.page.maxSize;;
        });
    };
}