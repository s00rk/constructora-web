angular.module('verallo').controller('CorreoViewController', CorreoViewController);
CorreoViewController.$inject = ['toaster', '$stateParams', '$state', '$scope', 'CorreoService'];

function CorreoViewController(toaster, $stateParams, $state, $scope, CorreoService) {
	var folder = $stateParams.folder === 'inbox' ? '' : $stateParams.folder;

	$scope.correo = new CorreoService();
    CorreoService.get({ 'id': $stateParams.mid, 'mailbox': folder }).$promise.then(function(mail) {
        $scope.mail = mail;
        $scope.correo.para = mail.desde.substring((mail.desde.indexOf('<')+1), mail.desde.length-1);
        $scope.correo.tema = 'RE: ' + mail.tema;
    });

    
    $scope.Responder = function()
    {
    	$scope.correo.es_respuesta = true;
		$scope.correo.$save({}, function(res){
			toaster.pop('success', '', 'Correo Enviado');
			$state.go('correo.folder');
		});
    };
}