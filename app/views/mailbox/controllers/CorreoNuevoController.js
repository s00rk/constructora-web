Verallo = angular.module('verallo');
Verallo.$inject = ['ngTagsInput'];
Verallo.controller('CorreoNuevoController', CorreoNuevoController);
CorreoNuevoController.$inject = ['toaster', '$rootScope', '$stateParams', '$state', '$scope', 'CorreoService', 'ApiServices', 'Upload'];

function CorreoNuevoController(toaster, $rootScope, $stateParams, $state, $scope, CorreoService, ApiServices, Upload) {

    $scope.correo = new CorreoService();
    $scope.para = null;
    $scope.files = [];
    
    $scope.addFiles = function(files)
    {
    	for(var i = 0; i < files.length; i++)
    		$scope.files.push( files[i] );
    	$scope.$apply();
    };
    $scope.removeItem = function(index)
    {
    	$scope.files.splice(index, 1);
    };


    $scope.enviar = function() 
    {
        if(!$scope.frmCorreo.$valid)
            return;

        $scope.correo.para = '';
        for(var i = 0; i < $scope.para.length; i++)
            $scope.correo.para += $scope.para[i].text + ',';
        $scope.correo.para = $scope.correo.para.substring(0, $scope.correo.para.length-1);

        if ($scope.files.length == 0) {
            $scope.correo.es_respuesta = false;
            $scope.correo.$save({}, function(res) {
                toaster.pop('success', '', 'Correo Enviado');
                $state.go('correo.folder');
            });
            return;
        }

        var emailUrl = ApiServices.get('emails');
        emailUrl = emailUrl.substring(0, emailUrl.length-4);

        var correo = $scope.correo;
        Upload.upload({
        	url: emailUrl,
        	data: {
        		file: $scope.files,
        		'tema': correo.tema,
        		'para': correo.para,
        		'cc': correo.cc,
        		'bcc': correo.bcc,
        		'mensaje': correo.mensaje,
        		'es_respuesta': false
        	}
        }).then(function (resp) {
            toaster.pop('success', '', 'Correo Enviado');
            $state.go('correo.folder');
        }, function (resp) {
            toaster.pop('success', '', 'Error al enviar el correo');
        }, function (evt) {
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
        });
    };
}