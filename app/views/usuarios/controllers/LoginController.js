angular.module('verallo').controller('LoginController', LoginController);

LoginController.$inject = ['$rootScope', '$state', '$scope', 'storage', 'AuthService'];
function LoginController($rootScope, $state, $scope, storage, AuthService) {
	$scope.usuario = {};
	$scope.login = {};

	var token = storage.getItem('TOKEN');

	if(token)
	{
		$state.go('inicio');
	}

	$scope.login = function()
	{
		if($scope.frmLogin.$invalid)
			return;
		AuthService.login({}, $scope.usuario, function(res){
			storage.setItem('TOKEN', res.token);
			window.location = '/';
		}, function(res){
			$scope.login.mensaje = 'Usuario/Contraseña Incorrecta';
		});
	};
}