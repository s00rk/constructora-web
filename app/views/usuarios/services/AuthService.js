var app = angular.module("verallo");
app.factory("AuthService", AuthService);
AuthService.$injects = ['$resource', 'ApiServices'];
function AuthService($resource, ApiServices){
	return $resource(ApiServices.auth,
		{
			id: "@id"
		},
		{
			'login': {
				method: 'POST',
				isArray: false
			}
		}
	);
};