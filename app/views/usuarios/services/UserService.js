var app = angular.module("verallo");
app.factory("UserService", UserService);
UserService.$injects = ['$resource', 'ApiServices'];
function UserService($resource, ApiServices){
	return $resource(ApiServices.get('usuarios'),
		{
			id: "@id"
		}
	);
};