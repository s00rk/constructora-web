angular.module('verallo').controller('PresupuestoIndexController', PresupuestoIndexController);

PresupuestoIndexController.$inject = ['toaster', '$state', '$scope', 'SERVER', 'PresupuestoService', 'CorreoService'];
function PresupuestoIndexController(toaster, $state, $scope, SERVER, PresupuestoService, CorreoService) {
	$scope.presupuestos = [];
	var tableS = null;
	var fe = null;
	$scope.SERVER = SERVER;
	var cargar = function()
	{
		if(tableS == null)
			return;
		var pagination = tableS.pagination;

		var start = pagination.start || 0;
		var number = pagination.number || 10;
		var data = {offset: start, limit: number};
		if("predicateObject" in tableS.search)
		{
			var q = tableS.search.predicateObject.$;
			data['query'] = q;
		}

		var fecha_inicio = $scope.fecha_inicio.split('/');
		var fecha_final = $scope.fecha_final.split('/');

		data['fecha_inicio'] = fecha_inicio[2] + '-' + fecha_inicio[1] + '-' + fecha_inicio[0];
		data['fecha_final'] = fecha_final[2] + '-' + fecha_final[1] + '-' + fecha_final[0];

		PresupuestoService.query(data).$promise.then(function (result) {
			$scope.presupuestos = result.map(function(o){
				var d = o;
				d.ganancia = parseFloat(d.total_porcentaje) - parseFloat(d.total);
				d.ganancia = Math.round(d.ganancia * 100) / 100;
				return d;
			})
			tableS.pagination.numberOfPages = Math.ceil(result.meta.total_count / number);
		});
		fe = true;
	}

	$scope.$watch('fecha_inicio', function(a, b){
		cargar();
	});
	$scope.$watch('fecha_final', function(a, b){
		cargar();
	});
	
	$scope.callServer = function(tableState) {
		tableS = tableState;
		var pagination = tableState.pagination;

		var start = pagination.start || 0;
		var number = pagination.number || 10;
		var data = {offset: start, limit: number};
		if("predicateObject" in tableState.search)
		{
			var q = tableState.search.predicateObject.$;
			data['query'] = q;
		}

		if(fe)
		{
			var fecha_inicio = $scope.fecha_inicio.split('/');
			var fecha_final = $scope.fecha_final.split('/');

			data['fecha_inicio'] = fecha_inicio[2] + '-' + fecha_inicio[1] + '-' + fecha_inicio[0];
			data['fecha_final'] = fecha_final[2] + '-' + fecha_final[1] + '-' + fecha_final[0];
		}
		PresupuestoService.query(data).$promise.then(function (result) {
			$scope.presupuestos = result.map(function(o){
				var d = o;
				d.ganancia = parseFloat(d.total_porcentaje) - parseFloat(d.total);
				d.ganancia = Math.round(d.ganancia * 100) / 100;
				return d;
			});			
			tableState.pagination.numberOfPages = Math.ceil(result.meta.total_count / number);

		});
	};

	$scope.edit = function(presupuesto)
	{
		$state.go('presupuesto_edit', {id: presupuesto.id});
	};

	$scope.new = function()
	{
		$state.go('presupuesto_new');
	};
	
	$scope.remove = function(index, presupuesto)
	{
		PresupuestoService.delete({ id: presupuesto.id });
		$scope.presupuestos.splice(index, 1);
		toaster.pop('success', 'Eliminar', 'Se ha eliminado el Presupuesto');
	};

	$scope.enviar = function(presupuesto)
	{
		var correo = new CorreoService();
		correo.presupuesto_id = presupuesto.id;
		correo.mensaje = 'Agradeciendo la presente y en espera de brindar mis servicios, quedo de usted para cualquier comentario.<br /><br /><br />Saludos,<br />Gracias.';
		correo.$save({}, function(){
			toaster.pop('success', '', 'Correo Enviado');
		});
	};
	$scope.cambioEstatus = function(presupuesto)
	{
		presupuesto.$update();
	}
}