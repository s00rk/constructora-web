angular.module('verallo').controller('PresupuestoController', PresupuestoController);

PresupuestoController.$inject = [
'toaster', 'ngDialog', '$state', '$scope', '$stateParams', 'ClienteService', 'PresupuestoService',
'ObraService', 'InsumoService', 'UnidadMedidaService', 'ObraItemService'
];
function PresupuestoController(toaster, ngDialog, $state, $scope, $stateParams, ClienteService, PresupuestoService, 
	ObraService, InsumoService, UnidadMedidaService, ObraItemService) {
	$scope.titulo = 'Nuevo Presupuesto';

	$scope.clientes = ClienteService.query();
	$scope.obras = [];
	$scope.ganancia = 0;
	ObraService.query().$promise.then(function(res){
		$.each(res, function(i, o){
			var ex = false;
			$.each($scope.obras, function(ii, oo){
				if(oo.id == o.id)
					ex = true;
			});
			if(!ex)
				$scope.obras.push({'id':o.id, 'nombre': o.nombre, 'items': o.items});
		});
	});
	$scope.insumos = InsumoService.query();
	$scope.unidad_medidas = UnidadMedidaService.query();
	
	if($stateParams.id != undefined)
	{
		$scope.presupuesto = new PresupuestoService();
		PresupuestoService.get({ id: $stateParams.id }).$promise.then(function(res){
			$scope.presupuesto = res;
			$scope.titulo = 'Editar Presupuesto: ' + res.nombre;
			$scope.contactos = res.cliente.contactos;

			var c = [];
			$.each(res.cliente.contactos, function(i, o){
				if(res.para != null && o.id != res.para.id)
					c.push(o);
			});
			$scope.contactos2 = c;

			ClienteService.get({ id: res.cliente.id }).$promise.then(function(r){
				$scope.sucursales = r.sucursales;
			});
			
		});
	}else{
		$scope.presupuesto = new PresupuestoService();
		$scope.presupuesto.obras = [];
		$scope.presupuesto.porcentaje = 20;
		$scope.presupuesto.iva = 0;
		$scope.presupuesto.total = 0;
		$scope.presupuesto.subtotal = 0;
	}

	var exito = function(data)
	{
		data.pdf = null;
		data.cliente = data.cliente.resource_uri;
		data.$update({}, function(r){
			toaster.pop('success', 'Guardar', 'Se ha guardado el Presupuesto');
			$state.go('presupuestos');
		});		
	};

	var error = function(data)
	{
		$.each(data.data, function(i, obj){
			$.each(obj, function(ii, objj){
				$.each(objj, function(j, k){
					toaster.pop('error', (ii.substr(0,1).toUpperCase()+ii.substr(1)), k);
				});
			});
		});
	};


	var calcularTotal = function()
	{
		$scope.presupuesto.subtotal = 0;
		$.each($scope.presupuesto.obras, function(i, o){
			$scope.presupuesto.subtotal += parseFloat(o.total);
		});
		$scope.presupuesto.iva = $scope.presupuesto.subtotal * .16;
		$scope.presupuesto.iva = Math.round($scope.presupuesto.iva * 100) / 100;
		$scope.presupuesto.total = $scope.presupuesto.subtotal + $scope.presupuesto.iva;
		$scope.presupuesto.total = Math.round($scope.presupuesto.total * 100) / 100;
	}

	$scope.guardar = function()
	{	
		if($scope.frmPresupuesto.$valid)
		{
			$scope.presupuesto.pdf = null;
			$scope.presupuesto.iva = $scope.presupuesto.iva.toString();
			$scope.presupuesto.total = $scope.presupuesto.total.toString();
			$scope.presupuesto.subtotal = $scope.presupuesto.subtotal.toString();

			$scope.presupuesto.cliente = $scope.presupuesto.cliente.resource_uri;
			if($scope.presupuesto.id == undefined)
			{
				$scope.presupuesto.$save({}, exito, error);
			}else{
				$scope.presupuesto.$update({}, exito, error);
			}
		}
	}

	var abrirModal = function(index, obra)
	{
		ngDialog.open({
			template: 'app/views/constructora/presupuestos/views/presupuesto-form-obra.html',
			scope: $scope,
			size: 'lg',
			backdrop: 'static',
			keyboard: false,
			controller: function($scope)
			{
				$scope.titulo_c = 'Nueva Obra';
				$scope.nueva_obra = {};
				$scope.nueva_obra.total = 0;
				$scope.items = [];
				$scope.insumos = InsumoService.query();
				$scope.unidad_medidas = UnidadMedidaService.query();
				
				if(obra != null)
				{
					var c = angular.copy(obra);
					$scope.nueva_obra = obra;
					$scope.titulo_c = 'Editar Obra: ' + c.nombre;
					$.each($scope.nueva_obra.items, function(i, o){
						o.insumo_s = o.insumo;
					});
					setTimeout(function(){
						$('span[aria-label="Select box activate"]').css('overflow', 'hidden');
						$scope.calcularObra();
					}, 500);
				}else{
					$scope.nueva_obra.items = [];
					$scope.nueva_obra.porcentaje = 20;
					$scope.nueva_obra.porcentaje_mano_obra = 60;
				}

				$scope.selectObra = function(item, obra)
				{
					if(item.id != 0)
					{
						$('input[aria-label="Select box"]').val(item.nombre);
						$('span[aria-label="Select box activate"]').css('overflow', 'hidden');
						var items = item.items.map(function(o){
							var obj = {};
							obj.insumo = o.insumo;
							obj.insumo_s = o.insumo;
							obj.importe = o.importe;
							obj.precio_unitario = o.precio_unitario;
							obj.cantidad = o.cantidad;
							obj.unidad_medida = o.unidad_medida;
							return obj;
						});
						$scope.nueva_obra.items = items;
						$scope.nueva_obra.total = 0;
						$.each($scope.nueva_obra.items, function(i, o){
							$scope.nueva_obra.total += parseFloat(o.importe);
						});
						$scope.nueva_obra.total = Math.round($scope.nueva_obra.total * 100) / 100; 
					}
				}

				$scope.getObras = function(search)
				{
					var nuevaObra = $scope.obras.slice();
					var ex = false;
					if(search)
					{
						$.each(nuevaObra, function(i, o){
							if(o.nombre.indexOf(search) != -1)
								ex = true;
						});
						if(!ex)
						{
							$.each($scope.obras, function(i, o){
								if("id" in o && o.id == 0)
									$scope.obras.splice(i, 1);
							});
							$scope.obras.unshift({'id': 0, 'nombre':search});
							return nuevaObra
						}
						

					}
					return nuevaObra;
				}

				$scope.agregarItem = function()
				{
					$scope.nueva_obra.items.push({});
				}
				$scope.deleteItem = function(r)
				{
					if(r.id != undefined)
						ObraItemService.delete({id: r.id});
					var i = $scope.nueva_obra.items.indexOf(r);
					$scope.nueva_obra.items.splice(i, 1);
					$scope.calcularObra();
				}
				$scope.guardarObra = function()
				{
					if($scope.frmObra.$invalid)
						return;

					if(index != null)
					{
						$scope.presupuesto.obras.splice(index, 1, $scope.nueva_obra);
					}else
						$scope.presupuesto.obras.push( $scope.nueva_obra );

					calcularTotal();
					ngDialog.close();
				}

				$scope.cerrar = function()
				{
					calcularTotal();
					ngDialog.close();
				}

				$scope.cambioItem = function(row)
				{
					row.insumo = row.insumo_s;
					row.unidad_medida = row.insumo.unidad_medida;
					row.precio_unitario = row.insumo.precio;
					row.cantidad = 1;
					row.importe = row.insumo.precio;
					$scope.cambioP(row);
				}

				$scope.cambioP = function(row)
				{
					row.importe = row.cantidad * row.precio_unitario;
					$scope.calcularObra();
				}

				$scope.calcularObra = function(){
					$scope.nueva_obra.total = 0;
					$.each($scope.nueva_obra.items, function(i, o){
						$scope.nueva_obra.total += parseFloat(o.importe);
					});

					var mando = 0;
					$.each($scope.nueva_obra.items, function(i, o){
						if(o.unidad_medida.codigo == 'JOR')
							mando += parseFloat(o.importe);
					});
					mando = Math.round((mando * 0.13) * 100) / 100;
					$scope.nueva_obra.mando = mando;
					$scope.nueva_obra.total += mando;

					var herreria = 0;
					$.each($scope.nueva_obra.items, function(i, o){
						if(o.insumo.herreria)
							herreria += parseFloat(o.importe);
					});
					herreria = Math.round((herreria * parseFloat($scope.nueva_obra.porcentaje_mano_obra/100)) * 100) / 100;
					$scope.nueva_obra.herreria = herreria;
					$scope.nueva_obra.total += herreria;

					var ganancia = 0;
					$.each($scope.nueva_obra.items, function(i, o){
						if(!o.insumo.herreria)
							ganancia += parseFloat(o.importe);
					});
					ganancia = Math.round((ganancia * parseFloat($scope.nueva_obra.porcentaje/100)) * 100) / 100;
					$scope.nueva_obra.ganancia = ganancia;
					$scope.nueva_obra.total += ganancia;

					$scope.nueva_obra.total = Math.round($scope.nueva_obra.total * 100) / 100;
				}
			}
		});
}

$scope.removeObra = function(index, obra)
{
	$scope.presupuesto.obras.splice(index, 1);

	calcularTotal();
}

$scope.new = function()
{
	abrirModal(null, null);
}
$scope.edit = function(index, row)
{
	abrirModal(index, row);
}

$scope.cambioCliente = function()
{
	ClienteService.get({ id: $scope.presupuesto.cliente.id }).$promise.then(function(res){
		$scope.presupuesto.cliente = res;
		$scope.contactos = res.contactos;
		$scope.contactos2 = [];
		$scope.presupuesto.para = null;
		$scope.presupuesto.contactos = null;
		$scope.sucursales = res.sucursales;
		console.log(res);
	});
}

$scope.cambioPara = function()
{
	$scope.presupuesto.contactos = null;
	var c = [];
	$.each($scope.contactos, function(i, o){
		if(o.id != $scope.presupuesto.para.id)
			c.push(o);
	});
	$scope.contactos2 = c;
}
}