var app = angular.module("verallo");
app.factory("ObraService", ObraService);
ObraService.$injects = ['$resource', 'ApiServices'];
function ObraService($resource, ApiServices){
	return $resource(ApiServices.get('obras'),
		{
			id: "@id"
		},
		{
			'update': { method:'PUT' }
		}
	);
};