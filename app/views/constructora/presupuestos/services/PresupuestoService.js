var app = angular.module("verallo");
app.factory("PresupuestoService", PresupuestoService);
PresupuestoService.$injects = ['$resource', 'ApiServices'];
function PresupuestoService($resource, ApiServices){
	return $resource(ApiServices.get('presupuestos'),
		{
			id: "@id"
		},
		{
			'update': { method:'PUT' },
			'patch': { method: 'PATCH' }
		}
	);
};