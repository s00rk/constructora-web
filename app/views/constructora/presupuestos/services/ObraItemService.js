var app = angular.module("verallo");
app.factory("ObraItemService", ObraItemService);
ObraItemService.$injects = ['$resource', 'ApiServices'];
function ObraItemService($resource, ApiServices){
	return $resource(ApiServices.get('obras_item'),
		{
			id: "@id"
		},
		{
			'update': { method:'PUT' }
		}
	);
};